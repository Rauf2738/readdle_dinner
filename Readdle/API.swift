//
//  API.swift
//  Readdle
//
//  Created by Рауф on 15.06.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class API{
    
   class func getName(completionHandler: @escaping (JSON)->()){
        Alamofire.request("https://sheets.googleapis.com/v4/spreadsheets/\(GoogleInfo.fileId)/values/A3:A34?majorDimension=ROWS&access_token=\(GoogleInfo.accessToken)").responseJSON{response in
            let json = JSON(response.value!)
            completionHandler(json)
        }
    }
    
    class func getSheet(completionHandler: @escaping (JSON)->()){
        var url = "https://sheets.googleapis.com/v4/spreadsheets/\(GoogleInfo.fileId)?access_token=".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        url = url + GoogleInfo.accessToken
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            let json = JSON(response.value!)
         completionHandler(json)
        })
    }
    
    class func getFoodId(humanId: Int, sheetTitle: String, completionHandler: @escaping (JSON)->()){
        var url = "https://sheets.googleapis.com/v4/spreadsheets/\(GoogleInfo.fileId)/values/\(sheetTitle)!B\(humanId+3):M\(humanId+3)?majorDimension=COLUMNS&access_token=".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        url = url + GoogleInfo.accessToken
        
        Alamofire.request(url).responseJSON{response in
            let json = JSON(response.value!)
            completionHandler(json)
        }
    }
    
    class func getMenu(sheetTitle: String, completionHandler: @escaping (JSON)->()){
        var url = "https://sheets.googleapis.com/v4/spreadsheets/\(GoogleInfo.fileId)/values/\(sheetTitle)!B2:M2?majorDimension=COLUMNS&access_token=".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        url = url + GoogleInfo.accessToken

        Alamofire.request(url).responseJSON{response in
            let json = JSON(response.value!)
            completionHandler(json)
        }
    }
}
