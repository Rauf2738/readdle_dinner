//
//  ViewController.swift
//  Readdle
//
//  Created by Рауф on 13.06.17.
//  Copyright © 2017 RGS. All rights reserved.

import UIKit
import GoogleSignIn

class SignIn: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var signOut: UIButton!
    let signInButton = GIDSignInButton.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        signInButton.center = view.center
        //signOut.addConstraint(NSLayoutConstraint(item: signOut, attribute: .top, relatedBy: .equal, toItem: signInButton, attribute: .notAnAttribute, multiplier: 1, constant: 50))
        view.addSubview(signInButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        //Имя из учетной записи аккаунта
        //GoogleInfo.name = user.profile.name
        

        GoogleInfo.name = "Рауф Гусейнов"

        print(" Access Token : " + signIn.currentUser.authentication.accessToken)
        GoogleInfo.accessToken = signIn.currentUser.authentication.accessToken
        let vc = storyboard?.instantiateViewController(withIdentifier: "GeneralView") as! GeneralView
        self.present(vc, animated: true, completion: {
            let data = NSData(contentsOf: user.profile.imageURL(withDimension: 400))
            if data != nil {
                vc.userImage.image = UIImage(data: data! as Data)
                vc.userName.text = user.profile.name
            }
        })
        //self.performSegue(withIdentifier: "ShowMenu", sender: self)
    }
}


