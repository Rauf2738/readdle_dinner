//
//  User.swift
//  Readdle
//
//  Created by Рауф on 13.06.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import Foundation
import Google
import GoogleSignIn

struct GoogleInfo {
    static var name : String {
        get{
            let plist = UserDefaults.standard
            plist.synchronize()
            return plist.value(forKey: "UserName") as! String
        }
        set{
            let plist = UserDefaults.standard
            plist.synchronize()
            plist.set(newValue, forKey: "UserName")
        }
    }
    
    static var accessToken : String {
        get{
            let plist = UserDefaults.standard
            plist.synchronize()
            return plist.value(forKey: "AcessToken") as! String
        }
        set{
            let plist = UserDefaults.standard
            plist.synchronize()
            plist.set(newValue, forKey: "AcessToken")
        }
    }
    
    static var Id : Int {
        get{
            let plist = UserDefaults.standard
            plist.synchronize()
            if let data = plist.value(forKey: "Id"){
                return data as! Int
            }
            return -1
        }
        set{
            let plist = UserDefaults.standard
            plist.synchronize()
            plist.set(newValue, forKey: "Id")
        }
    }
    
    static var fileId : String {
        get{
            return "15y8L-SHAEq7W6oKrzPGIxWZCOFUlnMUR65fJ95KB_Xc"
        }
    }
}
