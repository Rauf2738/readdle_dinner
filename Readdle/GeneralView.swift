//
//  ViewController.swift
//  Readdle
//
//  Created by Рауф on 13.06.17.
//  Copyright © 2017 RGS. All rights reserved.

import UIKit
import Google
import GoogleSignIn

class GeneralView: UIViewController {
    
    @IBOutlet weak var signOut: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userImage.layer.cornerRadius = userImage.frame.size.width/2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func SignOut(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignIn") as! SignIn
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func ShowOrder(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OrderShow") as! OrderShow
        self.present(vc, animated: true, completion: nil)
    }
}


