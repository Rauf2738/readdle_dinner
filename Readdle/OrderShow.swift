//
//  OrderShow.swift
//  Readdle
//
//  Created by Рауф on 22.06.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import Foundation
import UIKit

class OrderShow: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    let dayOfWeek = ["Понедельник" , "Вторник" , "Среда" , "Четверг" , "Пятница"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dayOfWeek[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dayOfWeek.count
    }
    
}
