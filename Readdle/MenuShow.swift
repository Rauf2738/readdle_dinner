//
//  MenuShow.swift
//  Readdle
//
//  Created by Рауф on 13.06.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MenuShow: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var userFood = [String]()
    var menu = [String]()
    var humanFood = [Int]()
    
    var sheetTitle = [String]() //Понедельник, Вторник, Среда, Четверг, Пятница
    var dayOfWeek = Int()
    @IBOutlet weak var label: UILabel!
    
    func getDayOfWeek()->Int? {
        let weekday = Calendar.current.component(.weekday, from:  Date()) - 2
        return weekday
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        dayOfWeek = getDayOfWeek()!
        
        API.getSheet(completionHandler: {response in
            self.parseSheet(data: response, completionHandler: {
                
                API.getName(completionHandler: {response in
                    self.parseNameId(data: response, completionHandler: {
                        
                        API.getMenu(sheetTitle: self.sheetTitle[self.dayOfWeek], completionHandler: {response in
                            self.parseMenu(data: response, completionHandler: {
                                
                                API.getFoodId(humanId: GoogleInfo.Id, sheetTitle: self.sheetTitle[self.dayOfWeek], completionHandler: {response in
                                    self.parseFoodId(data: response, completionHandler: {
                                        self.parseAllData()
                                    })
                                })})
                        })})
                })})
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func parseAllData(){
        for item in 0...menu.count-1{
            if(humanFood[item] == 1){
                userFood.append(menu[item])
            }
        }
        for item in 0...userFood.count-1{
            label.text = label.text! + userFood[item] + "\n"
        }
        activityIndicator.stopAnimating()
        GoogleInfo.Id = 0
    }
    
    func parseSheet(data: JSON, completionHandler : @escaping ()->()){
        var sheetId = String()
        
        let dataItem = data["sheets"]
        if(dataItem.isEmpty == false){
            for item in 0...4{
                sheetId = dataItem[item]["properties"]["title"].stringValue
                sheetTitle.append(sheetId)
                sheetId = String()
            }
            completionHandler()
        }
    }
    
    
    func parseNameId(data: JSON, completionHandler : @escaping ()->()){
        var name = String()
        
        let dataItem = data["values"]
        print(dataItem)
        if(dataItem.isEmpty == false){
            for item in 0...dataItem.count-1{
                name = dataItem[item][0].stringValue
                if(name == GoogleInfo.name){
                    GoogleInfo.Id = item
                    completionHandler()
                }
                name = String()
            }
            if(GoogleInfo.Id == -1){
                let alert = UIAlertController.init(title: "Ошибка", message: "Такого имени не существует", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: { (ACTION) in
                    self.activityIndicator.stopAnimating()
                    self.performSegue(withIdentifier: "ShowSignView", sender: self)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func parseFoodId(data: JSON, completionHandler : @escaping ()->()){
        var foodId = Int()
        let dataItem = data["values"]
        if(dataItem.isEmpty == false){
            for item in 0...11{
                if(dataItem[item][0] != JSON.null){
                    foodId = dataItem[item][0].intValue
                    humanFood.append(foodId)
                }
                else {
                    humanFood.append(0)
                }
                foodId = Int()
            }
            completionHandler()
        }
    }
    
    func parseMenu(data: JSON, completionHandler : @escaping ()->()){
        var menuFood = String()
        let dataItem = data["values"]
        if(dataItem.isEmpty == false){
            for item in 0...dataItem.count-1{
                menuFood = dataItem[item][0].stringValue
                menu.append(menuFood)
                menuFood = String()
            }
            completionHandler()
        }
    }
}
